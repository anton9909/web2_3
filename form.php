<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Лабораторная № 3</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="form">
      <div class="content">
        <h1 id="forms">Форма</h1>
        <form action=""
            method="POST">

        <p>Введите свое имя:</p>
        <label><br />
            <input name="field-name-1" />
          </label><br />

          <p>Введите Ваш e-mail:</p>
          <label><br />
            <input name="field-email"
              type="email"/>
          </label><br />

          <p id="birth">Ваша дата рождения:</p>
          <label><br />
            <select name="field-date">
              <option value="1995">1995</option>
              <option value="1996">1996</option>
              <option value="1997">1997</option>
              <option value="1998">1998</option>
              <option value="1999">1999</option>
              <option value="2000">2000</option>
              <option value="2001">2001</option>
              <option value="2002">2002</option>
            </select>
          </label><br />

          <p id="gender">Пол:</p>
          <label><input type="radio" checked="checked"
            name="radio-group-1" value="0" />М</label>
          <label><input type="radio"
            name="radio-group-1" value="1" />Ж</label><br />

            <p>Ваш IQ:</p>
          <label><input type="radio" checked="checked"
            name="radio-group-iq" value="0" />0</label>
          <label><input type="radio"
            name="radio-group-iq" value="-18" />-18</label>
          <label><input type="radio"
            name="radio-group-iq" value="180" />180</label>
          <label><input type="radio"
            name="radio-group-iq" value="100" />100</label><br />

            <p>  Ваши таланты:</p>
            <label>
                <select name="field-name-talents[]"
                  multiple="multiple">
                  <option value="Рисование">Рисование</option>
                  <option value="Пение" >Пение</option>
                  <option value="Попадание в неприятности" selected="selected">Попадание в неприятности</option>
                  <option value="Нахождение второго носка">Нахождение второго носка</option>
                </select>
              </label><br />

            <p>Расскажите о себе:</p>
            <label>
            <textarea name="field-name-4">Напишите что-нибудь о себе</textarea>
            </label><br />

            <p id="subm">Даю согласие на обратоку данных</p>
          <label><input type="checkbox" checked="checked"
            name="check-1" />Подтверждаю</label><br />

            <input type="submit" value="Отправить" />
          </form>

            <p><a id="bottom"></a></p>
        </div>
      </div>
    
  </body>
</html>
